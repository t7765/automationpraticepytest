from selenium import webdriver
from selenium.webdriver.chrome.options import Options

#from ..Config.config import TestConfig
#from ..Pages.DisciplinePage import DisciplinePage


class BaseTest:

    def setup(self):
        # for execute in background
        options = Options()
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        options.headless = True
        self.driver = webdriver.Chrome(chrome_options=options)
        #self.driver = webdriver.Chrome()
        # para sempre abrir um navegador com o usuario ja logado
        #chrome_options = Options()
        # chrome_options.add_argument("--user-data-dir=chrome-data")
        #self.driver = webdriver.Chrome(options=chrome_options)
        #self.driver.maximize_window()

    def teardown(self):
        self.driver.close()
