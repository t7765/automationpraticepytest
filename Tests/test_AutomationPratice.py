from pytest_check import check

from ..Pages.HomePage import HomePage
from ..Pages.SearchPage import SearchPage
from ..Pages.CartPage import CartPage

from ..Tests.test_Base import BaseTest


class TestAutomation(BaseTest):

    #CT 1: Verificar menus Women, dresses e Tshirt
    def test_check_menus(self):
        self.home = HomePage(self.driver)
        self.home.check_menu()
        
    #CT 2: Verificar submenu Women
    def test_check_menu_woman(self):
        self.home = HomePage(self.driver)
        self.home.check_submenu_women()
        
    #CT 3: Pesquisar por um vestido
    def test_search_dress(self):
        self.home = HomePage(self.driver)
        self.search = SearchPage(self.driver)
        self.home.search_product("Printed Dresses")
        self.search.check_product()
    
    #CT 4: Pesquisar por um vestido e filtrar pelo menor preço    
    def test_filter_low_price(self):
        self.home = HomePage(self.driver)
        self.search = SearchPage(self.driver)
        self.home.search_product("Printed Dresses")
        self.search.filter_low_price()
       
    # CT 5: Adicionar um produto no carrinho
    def test_add_product_cart(self):
        self.home = HomePage(self.driver)
        self.home.search_product("Blouse")
        self.search = SearchPage(self.driver)
        self.search.mouse_hover_img()
        self.cart = CartPage(self.driver)
        self.cart.add_to_cart()
    
    # CT 6: Remover produto do carrinho
    def test_remove_product_cart(self):
        self.home = HomePage(self.driver)
        self.home.search_product("Blouse")
        self.search = SearchPage(self.driver)
        self.search.mouse_hover_img()
        self.cart = CartPage(self.driver)
        self.cart.add_to_cart()
        self.cart.remover_product_cart()

        