from selenium.webdriver.common.by import By
from pytest_check import check

from ..Pages.BasePage import BasePage


class HomePage(BasePage):

    # VARIAVEIS
    MENU_WOMEN = (By.CSS_SELECTOR, "li > a[title='Women']")
    MENU_DRESSES = (By.CSS_SELECTOR, "#block_top_menu > ul > li> a[title='Dresses']")
    MENU_TSHIRTS = (By.CSS_SELECTOR, "#block_top_menu > ul > li> a[title='T-shirts']")
    SUBMENU_W_TOP = (By.CSS_SELECTOR, "li.sfHover > ul > li > a[title='Tops']")
    SUBMENU_TOP_TSHIRT = (By.CSS_SELECTOR, "li.sfHover > ul > li > ul > li > a[title='T-shirts']")
    SUBMENU_TOP_BLOUSES = (By.CSS_SELECTOR, "li.sfHover > ul > li > ul > li > a[title='Blouses']")
    SUBMENU_W_DRESSES = (By.CSS_SELECTOR, "li.sfHover > ul > li > a[title='Dresses']")
    SUBMENU_DRESSES_CASUALDRESSES = (By.CSS_SELECTOR, "li.sfHover > ul > li > ul > li > a[title='Casual Dresses']  ")
    SUBMENU_DRESSES_EVENINGDRESSES = (By.CSS_SELECTOR, "li.sfHover > ul > li > ul > li > a[title='Evening Dresses']")
    SUBMEN_DRESSES_SUMMERDRESSES = (By.CSS_SELECTOR, "li.sfHover > ul > li > ul > li > a[title='Summer Dresses']")
    SEARCH = (By.ID, "search_query_top")
    BTN_SEARCH = (By.CSS_SELECTOR, "button[name='submit_search']")
    URL = "http://automationpractice.com/index.php"


    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(self.URL)
        
    def check_menu(self):
        women = self.get_text(self.MENU_WOMEN)
        dresses = self.get_text(self.MENU_DRESSES)
        tshirts = self.get_text(self.MENU_TSHIRTS)
        with check:
            assert women == "WOMEN", "O nome retornado não está correto"
        with check:
            assert dresses == "DRESSES", "O nome retornado não está correto"
        with check:
            assert tshirts == "T-SHIRTS", "O nome retornado não está correto"
            
    def check_submenu_women(self):
        self.hover(*self.MENU_WOMEN)
        top = self.get_text(self.SUBMENU_W_TOP)
        tshirt = self.get_text(self.SUBMENU_TOP_TSHIRT)
        blouses = self.get_text(self.SUBMENU_TOP_BLOUSES)
        dresses = self.get_text(self.SUBMENU_W_DRESSES)
        casual = self.get_text(self.SUBMENU_DRESSES_CASUALDRESSES)
        evening = self.get_text(self.SUBMENU_DRESSES_EVENINGDRESSES)
        summer = self.get_text(self.SUBMEN_DRESSES_SUMMERDRESSES)
        with check:
            assert top == "TOPS"
        with check:
            assert tshirt == "T-shirts"
        with check:
            assert blouses == "Blouses"
        with check:
            assert dresses == "DRESSES"
        with check:
            assert casual == "Casual Dresses"
        with check:
            assert evening == "Evening Dresses"
        with check:
            assert summer == "Summer Dresses"
        
    def search_product(self, productName):
        self.send_keys(self.SEARCH, productName)
        self.click(self.BTN_SEARCH)
        
