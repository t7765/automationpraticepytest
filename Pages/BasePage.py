from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains


class BasePage:

    """ É o construtor do Python"""
    def __init__(self, driver):
        self.driver = driver

    # CRIAR OS METODOS UTILIZADOS NAS PAGINAS
    
    def click(self, locator):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)).click()

    def send_keys(self, locator, text):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)).send_keys(text)

    def clear(self, locator):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)).clear()

    def get_text(self, locator):
        return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)).text

    def get_text_js(self, locator):
        return self.driver.execute_script('return document.querySelector({}).value'.format(locator))

    def wait_clickable(self, locator):
        WebDriverWait(self.driver, 15).until(EC.element_to_be_clickable(locator))

    def wait_invisible(self, locator):
        WebDriverWait(self.driver, 15).until(EC.invisibility_of_element_located(locator))

    def ctrl_a(self, locator):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(locator)).send_keys(Keys.CONTROL + 'a')
        
    def hover(self, *locator):
        element_to_hover_over = self.driver.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element_to_hover_over)
        hover.perform()
        
    def element(self, *locator):
        return self.driver.find_element(*locator)
    
    def list_elements(self, *locator):
        WebDriverWait(self.driver, 15).until(EC.presence_of_element_located((locator)))
        return self.driver.find_elements(*locator)
    
    def get_value(self, *locator):
        element = self.driver.find_element(*locator)
        return element.get_attribute('src')
