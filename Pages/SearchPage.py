from selenium.webdriver.common.by import By
from pytest_check import check

from ..Pages.BasePage import BasePage


class SearchPage(BasePage):

    # VARIAVEIS
    URL = "http://automationpractice.com/index.php"
    RESULTS_FOUND_COUNT = (By.CSS_SELECTOR, "#center_column > h1 > span.heading-counter")
    NO_RESULTS_FOUND = "0 results have been found."
    PRICE_BEFORE = 1.0
    FIRST_PRODUCT_IMG = (By.XPATH, '//*[@id="center_column"]/ul/li[1]//*[@src]')
    LIST_NAME_PRODUCTS = ('//*[@id="center_column"]/ul/li[%s]/div/div[2]/h5/a')
    LIST_PRODUCT_IMG = ('//*[@id="center_column"]/ul/li[%s]//*[@src]')
    FILTER_LOW_PRICE = (By.CSS_SELECTOR, "#selectProductSort > option:nth-child(3)")
    LIST_PRICE = (By.XPATH, '//*[@id="center_column"]/ul/li/div/div[1]/div/div[2]/span[1]')
    PRICE_PRODUCTS = ('//*[@id="center_column"]/ul/li[%s]/div/div[1]/div/div[2]/span[1]')
    LIST_PRODUCTS_FOUND = (By.XPATH, '//*[@id="center_column"]/ul/li/div/div[2]/h5/a')
    LIST_ALL_PRODUCT_IMG = (By.XPATH, '//*[@id="center_column"]/ul/li//*[@src]')


    def __init__(self, driver):
        super().__init__(driver)
    
    def check_product(self):
        result = self.get_text(self.RESULTS_FOUND_COUNT)
        count = self.list_elements(*self.LIST_PRODUCTS_FOUND)
        # check products name
        for i in count:
           n = count.index(i) + 1
           name_products = self.element(By.XPATH, self.LIST_NAME_PRODUCTS % n).text
        if name_products == '':
            assert False, "O nome esta vazio"
        count_img = self.list_elements(*self.LIST_ALL_PRODUCT_IMG)
        # check products image
        for i in count_img:
            n = count_img.index(i) + 1
            a = self.get_value(By.XPATH, self.LIST_PRODUCT_IMG % n)
            if a == '':
                assert False, "A imagem esta vazia" 
        assert self.RESULTS_FOUND_COUNT != result
        
    def filter_low_price(self):
        self.click(self.FILTER_LOW_PRICE)
        count = self.list_elements(*self.LIST_PRICE)
        price_before = 0.0
        for x in count:
            n = count.index(x) + 1
            preco_atual = self.element(By.XPATH, self.PRICE_PRODUCTS % n ).get_attribute('innerText')
            preco_formatado = float(preco_atual.replace('$', ''))
            if price_before < preco_formatado:
                assert False, "Os preços não estão ordenados corretamente"
            price_before = preco_formatado
            
    def mouse_hover_img(self):
        self.hover(*self.FIRST_PRODUCT_IMG)
             
        