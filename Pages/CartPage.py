from pickle import FALSE
from selenium.webdriver.common.by import By
from pytest_check import check

from ..Pages.BasePage import BasePage


class CartPage(BasePage):
    
    BTN_ADD_CART = (By.XPATH, '//*[@id="center_column"]/ul/li[1]/div/div[2]/div[2]/a[1]/span')
    FEEDBACK_ADD_CART = (By.CSS_SELECTOR, 'div.layer_cart_product.col-xs-12.col-md-6 > h2')
    COUNT_ITEM_CART = (By.CSS_SELECTOR, "a > span.ajax_cart_quantity")
    CLOSE_CART = (By.CSS_SELECTOR, "span[title='Close window']")
    BTN_CART = (By.CSS_SELECTOR, "a[title='View my shopping cart']")
    BTN_TRASH = (By.CSS_SELECTOR, "i.icon-trash")
    FEEDBACK_CART_EMPTY = (By.CSS_SELECTOR, "#center_column > p")
    CART_EMPTY = (By.CSS_SELECTOR, "a > span.ajax_cart_no_product")
    
    def __init__(self, driver):
        super().__init__(driver)
    
    
    def add_to_cart(self):
        self.click(self.BTN_ADD_CART)
        Feedback = self.get_text(self.FEEDBACK_ADD_CART)
        Quantity = self.get_text(self.COUNT_ITEM_CART)
        self.click(self.CLOSE_CART)
        with check:
            assert Feedback == 'Product successfully added to your shopping cart'
        with check:
            assert Quantity == "1"
            
    def remover_product_cart(self):
        self.click(self.BTN_CART)
        self.click(self.BTN_TRASH)
        feedback = self.get_text(self.FEEDBACK_CART_EMPTY)
        Quantity = self.get_text(self.CART_EMPTY)
        with check:
            assert feedback == "Your shopping cart is empty."
        with check:
            assert Quantity == "(empty)"

        
        